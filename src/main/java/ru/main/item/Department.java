package ru.main.item;

/**
 * Класс
 */
public class Department
{
    private String code;
    private String job;
    private String description;

    public Department(String code, String job)
    {
        this.code = code;
        this.job = job;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getJob()
    {
        return job;
    }

    public void setJob(String job)
    {
        this.job = job;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    @Override
    public boolean equals(Object o)
    {
        if( this == o )
        {
            return true;
        }
        if( o == null || getClass() != o.getClass() )
        {
            return false;
        }

        Department that = ( Department ) o;

        if( !code.equals( that.code ) )
        {
            return false;
        }
        else if( !job.equals( that.job ) )
        {
            return false;
        }

        if( description != null && !description.equals( that.description ) )
        {
            return false;
        }
        else if( that.description != null && !that.description.equals( description ) )
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    @Override
    public int hashCode()
    {
        int result = code.hashCode();
        result = 31 * result + job.hashCode();
        return result;
    }

    @Override
    public String toString()
    {
        return "{" +
               "\"Department\": {" +
               "\"code\":\"" + this.code + "\", " +
               "\"job\":\"" + this.job + "\"," +
               "\"description\":\"" + this.description + "\"}" +
               "}";
    }
}
