package ru.main.item;

public class ConverterException extends Exception
{
    /**
     * Исключение с сообщением и причиной
     * @param message сообщение
     * @param cause причина
     */
    public ConverterException(String message, Throwable cause)
    {
        super( message, cause );
    }

    /**
     * Исключение с сообщением
     * @param message сообщением
     */
    public ConverterException(String message)
    {
        super( message );
    }
}
