package ru.main.tools;

import com.mysql.jdbc.StringUtils;
import org.apache.log4j.Logger;
import ru.main.init.AppProperties;
import ru.main.item.ConverterException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Класс отвечает за соединение с БД
 */
public class ConnectionTool
{
    /**
     * Инициализирует Connection к MySQL.
     * Отключает в нём автокоммит.
     *
     * @return возвращает Connection или null
     */
    public static Connection getConnection() throws ConverterException
    {
        String dbUrl = AppProperties.getProperty( "db.url" );
        String dbUser = AppProperties.getProperty( "db.user" );
        String dbPswd = AppProperties.getProperty( "db.pswd" );

        if( StringUtils.isEmptyOrWhitespaceOnly( dbUrl ) ||
            StringUtils.isEmptyOrWhitespaceOnly( dbUser ) ||
            StringUtils.isEmptyOrWhitespaceOnly( dbPswd ) )
        {
            throw new ConverterException( "Неверно указаны параметры подключения к БД в файле конфигурации." );
        }

        try
        {
            Class.forName( "com.mysql.jdbc.Driver" );
            return DriverManager.getConnection( dbUrl, dbUser, dbPswd );
        }
        catch ( SQLException e )
        {
            throw new ConverterException( "Не удалость установить подключение к БД", e );
        }
        catch ( ClassNotFoundException e )
        {
            throw new ConverterException( "Не удалось подключть драйвер БД", e );
        }
    }

    public static void rollback(Connection con) throws ConverterException
    {
        if( con == null )
        {
            throw new ConverterException( "Потеряно соединение с БД" );
        }
        try
        {
            con.rollback();
        }
        catch ( SQLException e )
        {
            throw new ConverterException( "Ошибка во время rollback операции", e );
        }

    }

    public static void commit(Connection con) throws ConverterException
    {
        if (con == null )
        {
            throw new ConverterException( "Потеряно соединение с БД" );
        }
        try
        {
            con.commit();
        }
        catch ( SQLException e )
        {
            throw new ConverterException( "Не удалось записать данные в БД", e );
        }
    }

    public static void setAutoCommit(Boolean value, Connection con) throws ConverterException
    {
        if (con == null )
        {
            throw new ConverterException( "Потеряно соединение с БД" );
        }

        try
        {
            con.setAutoCommit( value );
        }
        catch ( SQLException e )
        {
            throw new ConverterException( "Не поправить значение AutoCommit", e );
        }
    }


    public static void close(Connection con) throws ConverterException
    {
        if (con == null )
        {
            throw new ConverterException( "Потеряно соединение с БД" );
        }

        try
        {
            con.close();
        }
        catch ( SQLException e )
        {
            throw new ConverterException( "Не удалось закрыть соединение с БД", e );
        }
    }
}
