package ru.main.tools;

import com.mysql.jdbc.StringUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import ru.main.item.ConverterException;
import ru.main.item.Department;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Класс инициализатор
 */
public class XmlTool
{
    final static Logger log = Logger.getLogger( XmlTool.class );

    /**
     * Записывает Document в файл на системе.
     *
     * @param doc      Document которые будет записываться в файл.
     * @param filepath Путь к файлу, в который следует записать.
     *                 Файл будет создан в случае, если его нет
     * @return Возвращает true если файл записан удачно или false в обратном случае
     */
    public static void writeDocToFile(Document doc, String filepath) throws ConverterException
    {
        if( doc == null )
        {
            return;
        }

        try
        {
            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer transformer = transFactory.newTransformer();
            DOMSource domSource = new DOMSource( doc );
            StreamResult result = new StreamResult( new File( filepath ) );
            transformer.transform( domSource, result );
        }
        catch ( TransformerException e )
        {
            throw new ConverterException( "Ошибка при преобразовании документа в файл", e );
        }
    }

    /**
     * Преобразует Set<Department> в Document
     *
     * @param depSet
     * @return Возвращает Document или, в случае ошибки, null.
     */
    public static Document convertSetToDoc(Set<Department> depSet) throws ConverterException
    {
        if( depSet.size() == 0 )
        {
            return null;
        }
        log.info( "Запись коллекции в документ" );
        Document doc = null;
        try
        {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbf.newDocumentBuilder();
            doc = docBuilder.newDocument();

            Element departmentElement = doc.createElement( "department" );
            doc.appendChild( departmentElement );
            for ( Department dep : depSet )
            {
                Element itemElement = doc.createElement( "item" );
                itemElement.setAttribute( "code", dep.getCode() );
                itemElement.setAttribute( "job", dep.getJob() );

                if( !StringUtils.isEmptyOrWhitespaceOnly( dep.getDescription() ) )
                {
                    Element descriptionElement = doc.createElement( "description" );
                    descriptionElement.appendChild( doc.createTextNode( dep.getDescription() ) );
                    itemElement.appendChild( descriptionElement );
                }
                departmentElement.appendChild( itemElement );
            }
        }
        catch ( ParserConfigurationException e )
        {
            throw new ConverterException( "Ошибка при формировании документа", e );
        }
        log.info( "Запись завершена" );
        return doc;
    }

    /**
     * @param file - файл для конвертации
     * @return - возвращает Document для дальнейшего парсинга
     */
    public static Document convertFileToDoc(File file) throws ConverterException
    {
        if( file == null || !file.isFile() )
        {
            return null;
        }

        log.info( "Конвертирование файла в документ" );
        Document doc = null;
        try
        {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            doc = builder.parse( file );
        }
        catch ( IOException | SAXException | ParserConfigurationException e )
        {
            throw new ConverterException( "Не удалось сформировать документ для записи", e );
        }
        log.info( "Конвертирование завершено" );
        return doc;
    }

    /**
     * Конвертирует Document в Set<Department>
     * Так же проверяет на наличие дубликатов.
     *
     * @param doc Document на входе
     * @return Возвращает Set<Department> наполненый значениями взятыми из Document,
     * может верунть пуст сет.
     */
    public static Set<Department> convertDocToSet(Document doc) throws ConverterException
    {
        Set<Department> depSet = new HashSet<>();

        if( doc == null )
        {
            return depSet;
        }
        log.info( "Конвертирование документа в коллекцию" );


        if( !doc.hasChildNodes() )
        {
            return depSet;
        }

        if( !"department".equals( doc.getFirstChild().getNodeName() ) )
        {
            return depSet;
        }

        NodeList nodeList = doc.getFirstChild().getChildNodes();

        String duplicateValues = "";
        for ( int i = 0; i < nodeList.getLength(); i++ )
        {
            Node itemNode = nodeList.item( i );

            if( !"item".equals( itemNode.getNodeName() ) )
            {
                continue;
            }

            if( !itemNode.hasAttributes() )
            {
                continue;
            }

            Node codeNode = itemNode.getAttributes().getNamedItem( "code" );
            Node jobNode = itemNode.getAttributes().getNamedItem( "job" );

            if( codeNode == null || jobNode == null )
            {
                continue;
            }

            String depCode = codeNode.getNodeValue();
            String depJob = jobNode.getNodeValue();

            if( StringUtils.isEmptyOrWhitespaceOnly( depCode ) ||
                StringUtils.isEmptyOrWhitespaceOnly( depJob ) )
            {
                continue;
            }

            Department dep = new Department( depCode, depJob );

            NodeList itemChildNodes = itemNode.getChildNodes();
            for ( int j = 0; j < itemChildNodes.getLength(); j++ )
            {
                Node itemChild = itemChildNodes.item( j );

                if( "description".equals( itemChild.getNodeName() ) )
                {
                    if( itemChild.hasChildNodes() )
                    {
                        dep.setDescription( itemChild.getFirstChild().getNodeValue() );
                    }
                }
            }

            if( depSet.contains( dep ) )
            {
                throw new ConverterException( "В файле найдены дублированные значения: \n" + dep.toString() );
            }
            else
            {
                depSet.add( dep );
            }
        }
        log.info( "Конвертирование завешено" );
        return depSet;
    }
}
