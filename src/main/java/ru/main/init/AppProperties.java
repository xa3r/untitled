package ru.main.init;

import org.apache.log4j.Logger;
import ru.main.item.ConverterException;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Класс настроек программы.
 */
public class AppProperties
{
    private static Properties prop;
    final static Logger log = Logger.getLogger( AppProperties.class );

    public AppProperties(String filepath) throws ConverterException
    {
        prop = new Properties();

        try ( InputStream input = new FileInputStream( filepath ) )
        {
            prop.load( input );
        }
        catch ( IOException e )
        {
            throw new ConverterException( "Ошибка загрузки файла конфигурации", e );
        }
    }

    /**
     * @return возращает файл настроек
     */
    public static Properties getProp()
    {
        return prop;
    }

    /**
     * @param key Ключ поля на входе
     * @return Возвращает значение по ключу
     */
    public static String getProperty(String key)
    {
        return prop.getProperty( key );
    }

}
