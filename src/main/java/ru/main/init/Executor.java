package ru.main.init;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import ru.main.dao.DepartmentDAO;
import ru.main.item.ConverterException;
import ru.main.item.Department;
import ru.main.tools.ConnectionTool;
import ru.main.tools.XmlTool;

import java.io.File;
import java.sql.Connection;
import java.util.HashSet;
import java.util.Set;

/**
 * Исполнитель
 */
public class Executor
{
    final static Logger log = Logger.getLogger( Executor.class );

    /**
     * Загружает данные в таблицу
     *
     * @param filepath путь к файлу из котрого беруться данные
     * @throws ConverterException
     */
    public static void loadToTable(String filepath) throws ConverterException
    {

        File file = new File( filepath );

        if( !file.isFile() )
        {
            throw new ConverterException( "Не обнаружен файл" );
        }
        else if( file.length() == 0 )
        {
            throw new ConverterException( "Файл пуст" );
        }


        Connection con = ConnectionTool.getConnection();
        ConnectionTool.setAutoCommit( false, con );

        Document doc = XmlTool.convertFileToDoc( file );
        Set<Department> insValues = XmlTool.convertDocToSet( doc );

        if( insValues.size() == 0 )
        {
            log.info( "Нет информации для обработкм" );
            return;
        }

        DepartmentDAO departmentDao = new DepartmentDAO( con );
        Set<Department> oldValues = departmentDao.getDepartments();
        Set<Department> updValues = new HashSet<>();
        Set<Department> delValues = new HashSet<>( oldValues );

        log.info( "Ищем что бы поменять в таблице" );

        delValues.removeAll( insValues );
        insValues.removeAll( oldValues );

        for ( Department delDep : delValues )
        {
            String delDesc = delDep.getDescription();
            for ( Department newDep : insValues )
            {
                String newDesc = newDep.getDescription();

                if( ( delDesc != null && !delDesc.equals( newDesc ) ) ||
                    ( newDesc != null && !newDesc.equals( delDesc ) ) )
                {
                    updValues.add( newDep );
                    insValues.remove( newDep );
                    delValues.remove( delDep );
                }
            }
        }

        if( delValues.size() == 0 &&
            updValues.size() == 0 &&
            insValues.size() == 0 )
        {
            log.info( "Нет данных для изменений в таблице" );
            return;
        }

        try
        {
            if( delValues.size() > 0 )
            {
                departmentDao.deleteDepartments( delValues );
            }
            if( updValues.size() > 0 )
            {
                departmentDao.updateDepartments( updValues );
            }
            if( insValues.size() > 0 )
            {
                departmentDao.insertDepartments( insValues );
            }
            ConnectionTool.commit( con );
        }
        catch ( ConverterException e )
        {
            ConnectionTool.rollback( con );
            ConnectionTool.close( con );
            throw e;
        }
        ConnectionTool.close( con );
    }

    /**
     * Загружает данны в файл
     *
     * @param filepath путь к файлу в который произойдет загрузка
     * @throws ConverterException
     */
    public static void loadToFile(String filepath) throws ConverterException
    {
        Connection con = ConnectionTool.getConnection();
        try
        {
            DepartmentDAO departmentDao = new DepartmentDAO( con );
            Set<Department> set = departmentDao.getDepartments();
            if( set.size() > 0 )
            {
                Document doc = XmlTool.convertSetToDoc( set );
                XmlTool.writeDocToFile( doc, filepath );
            }
        }
        catch ( ConverterException e )
        {
            ConnectionTool.close( con );
            throw e;
        }
        ConnectionTool.close( con );
    }
}
