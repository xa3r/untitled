package ru.main;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import ru.main.init.AppProperties;
import ru.main.init.Executor;
import ru.main.item.ConverterException;

import java.io.File;

/**
 * Класс инициализатор
 */
public class Converter
{
    final static Logger log = Logger.getLogger( Converter.class );

    private static final String CONFIG_DIR_PATH = "./config/";
    private static final String APP_CONFIG_PATH = CONFIG_DIR_PATH + "config.properties";
    private static final String LOG_CONFIG_PATH = CONFIG_DIR_PATH + "log4j.properties";

    private static final String METHOD_TO_TABLE = "toTable";
    private static final String METHOD_TO_FILE = "toFile";

    /**
     * Конструктор класса инициализурет настройки программы и логера.
     * Запускает Executor на исполнение синхронизации.
     *
     * @param filepath
     */
    private Converter(String method, String filepath)
    {
        try
        {
            switch ( method )
            {
                case METHOD_TO_FILE:
                {
                    loadConfiguration();
                    log.info( "Программа запущена." );
                    Executor.loadToFile( filepath );
                    log.info( "Работа завершена с блестящим успехом" );
                }
                break;

                case METHOD_TO_TABLE:
                {
                    loadConfiguration();
                    log.info( "Программа запущена." );
                    Executor.loadToTable( filepath );
                    log.info( "Работа завершена с блестящим успехом" );
                }
                break;

                default:
                {
                    printArgumentError();
                    printHelp();
                }
                break;
            }
        }
        catch ( ConverterException e )
        {
            log.error( "Работа завершена с сокрушительным провалом", e );
        }
    }


    public static void loadConfiguration() throws ConverterException
    {
        if( !new File( APP_CONFIG_PATH ).isFile() )
        {
            throw new ConverterException( "Ошибка загрузки файла конфигурации: \n" + LOG_CONFIG_PATH );
        }
        else if( !new File( LOG_CONFIG_PATH ).isFile() )
        {
            throw new ConverterException( "Ошибка загрузки файла конфигурации: \n" + LOG_CONFIG_PATH );
        }
        else
        {
            new AppProperties( APP_CONFIG_PATH );
            PropertyConfigurator.configureAndWatch( LOG_CONFIG_PATH );
        }
    }

    /**
     * @param args аргументы, в данны момент может принимать только sync <путь к файлу>
     */
    public static void main(String[] args)
    {
        if( args.length == 2 )
        {

            new Converter( args[0], args[1] );
        }
        else
        {
            printArgumentError();
            printHelp();
        }
    }

    private static void printArgumentError()
    {
        System.out.println( "Неверно указаны аргументы" );
    }

    private static void printHelp()
    {
        System.out.println( "Используете:\n" +
                            "toTable <filepath> - для загрузки содержимого файла в таблицу\n" +
                            "toFile <filepath> - для загрузки содержимого таблицы в файлы" );
    }

}