package ru.main.dao;

import org.apache.log4j.Logger;
import ru.main.item.ConverterException;
import ru.main.item.Department;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Класс осуществляет работу с БД.
 */
public class DepartmentDAO
{
    final static Logger log = Logger.getLogger( DepartmentDAO.class );

    private final String TABLENAME = "Department";
    private Connection con;

    public DepartmentDAO(Connection con)
    {
        this.con = con;
    }

    /**
     * @return Возвращает Set с Department набранным из таблицы
     * Set может быть пустым.
     */
    public Set<Department> getDepartments()
            throws ConverterException
    {
        log.info( "Получем все данные из таблицы" );

        Set<Department> depSet = new HashSet<>();
        String query = "SELECT DepCode, DepJob, Description FROM " + TABLENAME + ";";
        try ( Statement s = con.createStatement() )
        {
            try ( ResultSet rs = s.executeQuery( query ) )
            {
                while ( rs.next() )
                {
                    Department dep = new Department( rs.getString( "DepCode" ),
                                                     rs.getString( "DepJob" ) );
                    dep.setDescription( rs.getString( "Description" ) );
                    depSet.add( dep );
                }
            }
            catch ( SQLException e )
            {
                throw new ConverterException( "Ошибка обращения к БД", e );
            }
        }
        catch ( SQLException e )
        {
            throw new ConverterException( "Ошибка обращения к БД", e );
        }

        log.info( depSet.size() > 0 ? "Данные получены" : "Таблица пуста" );
        return depSet;

    }

    /**
     * Стирает все значения из таблицы
     *
     * @return true в случае успеха, иначе false
     */
    private void cleanDepartmentTable() throws ConverterException
    {
        log.info( "Очистка таблицы" );
        String deleteQuery = "DELETE FROM Department;";
        try ( PreparedStatement ps = con.prepareStatement( deleteQuery ) )
        {
            ps.executeUpdate();
            log.info( "Таблица очищена" );
        }
        catch ( SQLException e )
        {
            throw new ConverterException( "Ошибка при обращения к БД", e );
        }
    }

    /**
     * Обновляет данные в БД
     *
     * @param values
     * @return true - Успех, false - провал
     */
    public void insertDepartments(Set<Department> values) throws ConverterException
    {
        if( values.size() == 0 )
        {
            return;
        }
        log.info( "Добавляем данные в таблицу" );
        String insertQuery = " INSERT INTO Department ( DepCode, DepJob, Description) VALUES (?,?,?);";

        try ( PreparedStatement ps = con.prepareStatement( insertQuery ) )
        {
            for ( Department dep : values )
            {
                ps.setString( 1, dep.getCode() );
                ps.setString( 2, dep.getJob() );
                ps.setString( 3, dep.getDescription() );
                ps.addBatch();
            }
            ps.executeBatch();
        }
        catch ( SQLException e )
        {
            throw new ConverterException( "Не удалось записать данные в таблицу ", e );
        }
        log.info( "Данные добавлены" );
    }

    public void updateDepartments(Set<Department> values) throws ConverterException
    {
        if( values.size() == 0 )
        {
            return;
        }
        log.info( "Обновляем данные в таблице" );
        String insertQuery = " UPDATE " + TABLENAME + " SET Description = ? WHERE DepCode = ? AND DepJob = ?;";

        try ( PreparedStatement ps = con.prepareStatement( insertQuery ) )
        {
            for ( Department dep : values )
            {
                ps.setString( 1, dep.getDescription() );
                ps.setString( 2, dep.getCode() );
                ps.setString( 3, dep.getJob() );
                ps.addBatch();
            }
            ps.executeBatch();
        }
        catch ( SQLException e )
        {
            throw new ConverterException( "Не удалось записать данные в таблицу ", e );
        }
        log.info( "Данные обновлены" );
    }


    public void deleteDepartments(Set<Department> values) throws ConverterException
    {
        if( values.size() <= 0 )
        {
            return;
        }
        log.info( "Удаляем данные из таблицы" );
        String insertQuery = "DELETE FROM Department WHERE DepCode = ? AND DepJob = ?; ";

        try ( PreparedStatement ps = con.prepareStatement( insertQuery ) )
        {
            for ( Department dep : values )
            {
                ps.setString( 1, dep.getCode() );
                ps.setString( 2, dep.getJob() );
                ps.addBatch();
            }
            ps.executeBatch();
        }
        catch ( SQLException e )
        {
            throw new ConverterException( "Не удалось удалить данные из таблицы", e );
        }

        log.info( "Данные удалены" );
    }
}
